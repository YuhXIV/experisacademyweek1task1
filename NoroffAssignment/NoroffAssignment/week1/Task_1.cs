﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    public class Task_1
    {
        public static void SayHello()
        {
            Console.WriteLine("Please enter your name:");
            string input = System.Console.ReadLine();
            Console.WriteLine($"Hello {input}, my name is Kjetil Huy Tran.");
        }
    }
}
