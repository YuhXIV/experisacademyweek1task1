﻿using System;

namespace NoroffAssignment.week1
{
    class Task_2
    {
        // The main function that will run task 2
        public static void NameCheck()
        {
            string input;
            bool isUpperCase;
            bool length = false;
            bool fLetter = false;
            Console.WriteLine("Please enter your name:");
            do
            {
                //prompt Console.Readline() as long as first letter is not uppercase
                input = Console.ReadLine().Trim();
                isUpperCase = UpperCaseName(input);
                if (!isUpperCase) Console.WriteLine("First letter must be uppercase");
            } while (!isUpperCase); 


            ConsoleKey response;
            do
            {
                //prompt questien of displaying input.
                Console.Write("Do you want to display the length of your name? [y/n] ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter) //if enter is pressed, nothing happen
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N); // only character y or n make you proceed

            if (response == ConsoleKey.Y) length = true;

            do
            {
                //promp question of first letter
                Console.Write("Do you want to display the first letter of your name? [y/n] ");
                response = Console.ReadKey(false).Key;
                if (response != ConsoleKey.Enter)
                    Console.WriteLine();

            } while (response != ConsoleKey.Y && response != ConsoleKey.N); // same as previous, can only procced if character y or n is entered

            if (response == ConsoleKey.Y) fLetter = true;

            Console.WriteLine(Text(input, length, fLetter));
        }

        static bool UpperCaseName(string name) // check for the first letter is uppercase
        {
            char[] tmp = name.ToCharArray();
            if ((int)tmp[0] > 64 && (int)tmp[0] < 91)
            {
                return true;
            }
            return false;
        }


        static char FirstLetter(string input) //return first letter of the input
        {
            char[] firstletter = input.ToCharArray();
            return firstletter[0];
        }

        static string Text(string name, bool length = false, bool firstletter = false)
        { // create the text string that is written in console when all the prompt is completed
            string text = $"Hello {name}"; 
            if (length || firstletter) text += ", your name";
            else text += ".";
            if (length)
            {
                text += $" is {name.Length} characters long";
                if (!firstletter) return text += ".";
                else text += " and";
            }
            if (firstletter)
            {
                text += $" starts with {AOrAn(IsVowel(name))} {FirstLetter(name)}.";
            }
            return text;
        }
        static bool IsVowel(string input)  //check the firstletter of the input is vowel
        {
            char letter = FirstLetter(input);
            char[] vowel = { 'a', 'e', 'o', 'u', 'y' };
            foreach (char c in vowel)
            {
                if (c.Equals(Char.ToLower(letter))) // check the vowel against the firstletter in input converted to lowercase
                {
                    return true;
                }
            }
            return false;
        }
        static string AOrAn(bool isVowel) //if the input value is true, return an, else return a
        {
            if (isVowel) return "an";
            return "a";
        }
    }
}
